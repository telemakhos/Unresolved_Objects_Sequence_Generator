# Generating unresolved target images
Outputs images of gaussian blobs moving in a noisy field.

**Motivation**
readme in progress...

[//]: # (Image References)
[image0]: ./imgs/random.gif    "15 unresolved objects with randomly assigned trajectories"
[image1]: ./imgs/converging.gif    "10 targets converging on a point"

---

#### Software Requirements
The following Python and packages were used:
* Python 3.6.1
* Numpy 1.13.1
* Scikit-Image 0.13.0

---

#### Code Usage
Running the code with the -h flag shows input options:
```
usage: gen_tracks.py [-h] [-s] [-p] [-w]

optional arguments:
  -h, --help   show this help message and exit
  -s, --show   show frames
  -p, --pause  pause the video for each final frame
  -w, --write  write final result frames to output directory

```
---

#### Results
(2 movies ... may take a few seconds to load)

![alt text][image0]
![alt text][image1]  
