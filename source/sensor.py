# -*- coding: utf-8 -*-

__author__  = 'drforester'
__date__    = '24 January 2018'
'''
SENSOR CLASS
'''

import numpy as np

class Sensor(object):
    
    def __init__(self):
        # sensor array size
        self.rows = 512
        self.cols = 640
        # sensor noise
        self.mu_noise    = 10 # gaussian noise mean
        self.sigma_noise = 3  # gaussian noise std