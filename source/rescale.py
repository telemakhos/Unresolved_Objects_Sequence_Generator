'''
rescale images in 'data_dir', then write to 'write_dir'.
After running this script, create a gif with the following BASH command:
  ~$ convert -delay 10 -loop 0 *.png ten_objs.gif
'''

import os
from skimage.transform import rescale
from skimage.io import imread, imsave

'''
   returns a file path generator for all files under data_dir
'''
def get_imgs(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter


# get all filetype files, then rescale (make 50% smaller)
data_dir = './output' # the dir contining the files to be renamed
write_dir = data_dir+'/rescaled'
filetype = '.png'     # the filetype to be renamed
fileiter = get_imgs(data_dir)
pngs = [f for f in fileiter if os.path.splitext(f)[1] == filetype]

fcount = 0
for x in pngs:
    print(x)
    img_name = x.split('/')[2]
    img = imread(x)
    sm_img = rescale(img, 0.50)
    save_name = write_dir + '/' + img_name 
    imsave(save_name ,sm_img)
fcount += 1

