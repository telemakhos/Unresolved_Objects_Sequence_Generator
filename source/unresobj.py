
# -*- coding: utf-8 -*-

__author__  = 'drforester'
__date__    = '19 February 2018'
__comment__ = 'add acceleration signs and magnitudes'
'''
UNRESOBJ CLASS
--------------
lengths, velocities, and accelerations are in pixels (no translation to
physical dimensions has been made)
'''

import random
import numpy as np

class Unresobj(object):

    def __init__(self):

        # initial acceleration sign
        self.acc_y_sign = random.choice([-1,1])
        self.acc_x_sign = random.choice([-1,1])

        # multipliers to scale acceleration magnitude
        self.acc_y_mult = np.random.uniform(0.0, 30.0, 1)
        self.acc_x_mult = np.random.uniform(0.0, 30.0, 1)

        # minumum initial acceleration       
        self.min_acc = np.random.uniform(0.5, 1.5, 1)
        # maximum initial acceleration
        self.max_acc = np.random.uniform(2.0, 7.5, 1)

        # minumum initial velocity       
        self.min_vel = np.random.uniform(75.0, 125.0, 1) 
        # maximum initial velocity
        self.max_vel = np.random.uniform(300.00, 500.00, 1)

        # target signal amplitude
        self.A = np.random.uniform(0.5, 2.0, 1)
        #self.A = 1.0

        # target vertical extent        
        self.sigma_y = np.random.uniform(0.75, 4.5, 1)
        # target horizontal extent 
        self.sigma_x = np.random.uniform(0.75, 4.5, 1)
        
        self.acc_y = 0.0
        self.acc_x = 0.0
    
    # define the object's vertical acceleration
    def get_y_acc(self, t):
        self.acc_y = self.acc_y_sign * self.acc_y_mult * t
        #self.acc_y = self.acc_y_sign * np.asscalar(np.sin(1000*t))

    # define the object's horizontal acceleration        
    def get_x_acc(self, t):
        self.acc_x = self.acc_x_sign * self.acc_x_mult * t
