# -*- coding: utf-8 -*-

__author__  = 'drforester'
__version__ = '0.1.1'
__date__    = '21 February 2018'

'''
outputs frames of a simulated unresolved object moving through a noisy
(gaussian) field. So far the only noise is temporal. A later version may
contain some structural noise, such as blurred cloud edges.

###############################################################
HOW TO USE:
 ~$ py35 main.py --help
usage: main.py [-h] [-n N] [-x] [-s] [-p] [-w]

optional arguments:
  -h, --help      show this help message and exit
  -n N            the number of objects to simulate
  -x, --crossing  create a trajectory intersection sceanrio
  -s, --showing   show image results for each processing step
  -p, --pause     pause at each frame; press enter to continue
  -w, --write     write final result frames to output directory

example usage to simulate 4 objects with intersecting trajectories. Using '-s'
to show the frames:  
 ~$ python3 main.py -n 4 -x -s

###############################################################
Notes:
*  To change sensor settings, such as FPA dimensions, noise model, etc, modify
   the respective fields in sensor.py
*  To change the unresolved object's appearance or motion model, modify the
   appropriate fields in unresobj.py


###############################################################
If you wrote frames to disc using the '-w' option, you can create
an mp4 video like this:
 ~$ avconv -start_number 0001 -i frame_%04d.png -b:v 1800k -r 10 ./vid.mp4
'''

#######################################################################
'''
IMPORTS
'''
import os
import argparse
import pickle
import numpy as np
#np.set_printoptions(precision=3)
np.set_printoptions(formatter={'float': '{: 0.3f}'.format})

from unresobj import Unresobj  # the unresolved objec (target) class
from sensor import Sensor      # the sensor (camera or other imaging-sensor) class
from target_positions import get_rand_trajs, get_xing_trajs
from build_image import build_img
from utils import custom_plots, csv_writer

#######################################################################
'''
Arguent Parsing
'''
parser = argparse.ArgumentParser()
parser.add_argument('-n', type=int, default=3,
                    help='the number of objects to simulate')
parser.add_argument('-x', '--crossing', action='store_true',
                    help='create a trajectory intersection sceanrio')
parser.add_argument('-s', '--showing', action='store_true',
                    help='show image results for each processing step')
parser.add_argument('-p', '--pause', action='store_true',
                    help='pause the video for each final frame')
parser.add_argument('-w', '--write', action='store_true',
                    help='write final result frames to output directory')

args = parser.parse_args()
xing = False
showImg = False
pauseImg = False
writeOut = False
nb_objs = args.n
if args.crossing: xing = True
if args.showing: showImg = True
if args.pause: pauseImg = True
if args.write: writeOut = True

#######################################################################
'''
FUNCTION DEFINITIONS
'''
# for printing numbers in aligned columns
def align(s1, s2, s3):
    return "{:<10s}{:<10s}{:<10s}".format(s1, s2, s3)


# returns a file path generator for all files under data_dir
def get_imgs(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter

#######################################################################

'''
SETTINGS
'''
print_results = True  # print (time, y-position, x-position) in terminal

time_secs = 0.9 # test duration (simulated) seconds
time_step = 0.01 # time-step increment in (simulated) seconds
                        
#######################################################################

'''
MAIN
'''

# delete any existing PNG files in the output dir
if writeOut:
    outdir = 'output'
    filetype = '.png'
    # delete any existing PNG files
    fileiter = get_imgs(outdir)
    pngs = [f for f in fileiter if os.path.splitext(f)[1] == filetype]
    for x in pngs:
        os.remove(x)

# make an array of times
times = np.arange(0.0, time_secs, time_step)

# instantiate sensor
sensor = Sensor()

if xing:
    # CROSSING TRAJECTORIES
    urobjs = []
    for ii in range(nb_objs):
        urobjs.append( Unresobj() )
    positions_array = get_xing_trajs(times, sensor, urobjs)
    # the returned positions array is of shape: (time_steps, nb_objs, 2)
    # create a list of (y,x) positions for these objects
    positions = []
    for iobj in range(nb_objs):
        positions.append(positions_array[:,iobj,:])
    

else:
    # RANDOMLY GENERATED TRAJECTORIES (no explicit crossings)
    # create a list of Unresobj objects and their (y,x) positions
    urobjs = []
    positions = []
    for ii in range(nb_objs):
        # instantiate an Unresobj and append to list
        urobjs.append( Unresobj() )
        # create a list of positions for this object and append
        positions.append( get_rand_trajs(times, sensor, urobjs[ii]) )


# propagate the time
if xing:
    time_steps = len(positions[0]) # all lengths the same
else:
    time_steps = times.shape[0]

for ii in range(time_steps):

    t = times[ii]
    objs_pos = [positions[iobj][ii] for iobj in range(nb_objs)]
    frame = build_img(objs_pos, sensor, urobjs)

    if writeOut:
        ifnum = str(ii).zfill(4)
        cm='gray'
        if ii==0: # set the scaling values only once
            vmin=np.min(frame)
            vmax=np.max(frame)
        # save the new images
        outname = 'frame_'+str(ifnum)+filetype 
        custom_plots.write_img(frame, outname, outdir, cm, vmin, vmax)
    if showImg:
        ifnum = str(ii).zfill(4)
        cm='gray'
        plot_title=ifnum
        if ii==0: # set the scaling values only once
            vmin=np.min(frame)
            vmax=np.max(frame)
        custom_plots.show_img(frame, plot_title, cm, vmin, vmax, ask=pauseImg)

################################################
# write truth serialized formats (.npy and .pkl)
################################################
if writeOut:
    # remove target positions lying outside the frame of dimensions rows x cols
    for iobj in range(nb_objs):
        yidx = np.where( (positions[iobj][:,0] < sensor.rows) & (positions[iobj][:,0] >= 0) )
        xidx = np.where( (positions[iobj][:,1] < sensor.cols) & (positions[iobj][:,1] >= 0) )
        idx = np.intersect1d(yidx, xidx)
        positions[iobj] = positions[iobj][idx]
    
    # create list of rows to be serialized
    truthPos = []
    truthId = []
    nb_unique_ids = 0
    longesttracklen = np.max( [ positions[iobj].shape[0] for iobj in range(nb_objs) ] )
    for ii in range(longesttracklen):
        t = times[ii]
        poslist = [ positions[iobj][ii]
                      for iobj in range(nb_objs)
                      if positions[iobj].shape[0] > ii ]
        idlist = [ iobj
                   for iobj in range(nb_objs)
                   if positions[iobj].shape[0] > ii ]
        if max(idlist) > nb_unique_ids:
            nb_unique_ids = max(idlist)
    
        truthPos.append( np.array(poslist) )
        truthId.append(idlist)
    
    np.save( 'truth_Pos', np.array(truthPos) )
    pickle.dump( truthId, open('truth_Ids.pkl', 'wb') )
    
    
    #########################
    # write truth to CSV file
    #########################
    idlist = [str(ii) for ii in range(nb_unique_ids)]
    header_row = ['frame'] + idlist
    csv_rows = []
    for ientry in range(len(truthPos)):
        csv_rows.append(['frame', ientry])
        for itarget in range(len(truthId[ientry])):
            csv_rows.append([ ' ', 'obj_id', truthId[ientry][itarget] ])
            csv_rows.append([ ' ', 'obj_pos',
                  np.array( truthPos[ientry][itarget][0]),
                  np.array( truthPos[ientry][itarget][1]) ])
        csv_rows.append( [''] )
    csv_writer.write([], csv_rows)
