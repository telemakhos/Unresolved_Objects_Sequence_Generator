# -*- coding: utf-8 -*-

__author__  = 'drforester'
__date__    = '13 February 2018'

import numpy as np

def build_img(positions, sensor, urobj):
    
    mu_noise = sensor.mu_noise
    sigma_noise = sensor.sigma_noise
    rows = sensor.rows
    cols = sensor.cols


    # create a frame with noise (temporal noise, no spatial noise)
    image = np.zeros((rows, cols))
    disp_img = image.copy()

    nb_objs = len(urobj)
    for iobj in range(nb_objs):
        A = urobj[iobj].A
        sigma_y = urobj[iobj].sigma_y
        sigma_x = urobj[iobj].sigma_x

        pos_y, pos_x = positions[iobj]
        
        # get a list of pixels surrounding a target
        max_sigma = int(max((sigma_y, sigma_x)))+1
        span = [x for x in range(-(max_sigma*3), (max_sigma*3)+1)]
        adjacency = [(i,j) for i in span for j in span]
        
        # get image (y,x) indexes of pixels surrounding the target which
        # are still within the image frame.
        yint = int(pos_y)
        xint = int(pos_x)
        box_inds = [ (y + yint, x + xint) for (y,x) in adjacency
                     if ( (y + yint) < rows and (x + xint) < cols and
                          (y + yint) > 0.0 and (x + xint) > 0.0 ) ]

        # calculate value of the gaussian in pixels near the object center
        for yi, xi in box_inds:
            g = A * np.exp( -1 * ( ((yi-pos_y)**2/(2*sigma_y**2)) +
                                   ((xi-pos_x)**2/(2*sigma_x**2)) ) )
            disp_img[yi, xi] += g * mu_noise

    noisy = np.random.normal(mu_noise, sigma_noise, rows*cols)
    noisy_img = np.reshape(noisy, (rows, cols))
    disp_img = disp_img + noisy_img
    disp_img[disp_img<0] = 0
        
    return disp_img
