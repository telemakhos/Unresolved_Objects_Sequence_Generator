# -*- coding: utf-8 -*-

__author__  = 'drforester'
__date__    = '21 February 2018'

import random
import numpy as np

def get_rand_trajs( times, sensor, urobj ):
    '''
    Creates waypoints for each object in list 'unrobj'.
    Each object has a randomly generated velocity and acceleration.
    '''
    min_acc = urobj.min_acc
    max_acc = urobj.max_acc

    min_vel = urobj.min_vel
    max_vel = urobj.max_vel

    A = urobj.A
    sigma_y = urobj.sigma_y
    sigma_x = urobj.sigma_x
    
    rows = sensor.rows
    cols = sensor.cols
    
    # initialize acc_x and acc_y
    sign_acc_y = np.random.choice([-1, 1], 1, p=[0.5, 0.5])[0]
    sign_acc_x = np.random.choice([-1, 1], 1, p=[0.5, 0.5])[0]
    acc_y = sign_acc_y * np.asscalar(np.random.uniform(min_acc, max_acc, 1))*0.03
    acc_x = sign_acc_x * np.asscalar(np.random.uniform(min_acc, max_acc, 1))*0.03

    # initialize vel_y and vel_x
    sign_vel_y = np.random.choice([-1, 1], 1, p=[0.3, 0.7])[0] # 7:3 more likely to go down than up
    sign_vel_x = np.random.choice([-1, 1], 1, p=[0.5, 0.5])[0]
    vel_y_0 = sign_vel_y * np.asscalar(np.random.uniform(min_vel, max_vel, 1))
    vel_x_0 = sign_vel_x * np.asscalar(np.random.uniform(min_vel, max_vel, 1))
    vel_y = vel_y_0
    vel_x = vel_x_0
    
    # generate a random starting position (not too close to frame border)
    numb_targets = 1
    offset = int(rows/6)
    sub_rows = rows-offset
    inds_y = np.random.random_integers(offset, rows-offset, numb_targets)
    inds_x = np.random.random_integers(offset, cols-offset, numb_targets)
    yx_pairs = [(inds_y[ii], inds_x[ii]) for ii in range(numb_targets)]
    yx_pairs = [x + np.random.rand(1) for x in yx_pairs] # a list of (y,x) floats

    pos_y_0, pos_x_0 = yx_pairs[0]    
    
    positions = np.zeros( (times.shape[0], 2), dtype=np.float32 )
    
    for ii in range(times.shape[0]):
        
        t = times[ii]
        
        # update the accelerations according to the motion model
        urobj.get_y_acc(t)
        urobj.get_x_acc(t)
        acc_y = sign_acc_y * urobj.acc_y
        acc_x = sign_acc_x * urobj.acc_x

        # update the velocity
        vel_y += acc_y * t
        vel_x += acc_x * t

        # calculate the current position; true center of the object
        pos_y = 0.5*acc_y*t**2 + vel_y*t + pos_y_0
        pos_x = 0.5*acc_x*t**2 + vel_x*t + pos_x_0
        
        positions[ii,:] = (pos_y, pos_x)
    
    return(positions)


def get_xing_trajs( times, sensor, urobjs ):
    '''
    Create waypoints for all objects in the list 'urobjs'.
    All trajectories intersect at a location 'xing'; that is,
    there is one frame in which all target centers occupy the
    same point. Accelerations are constant. Velocities are
    determined by the distance that must be crossed to reach
    the intersection point in the pre-determined number of
    time steps.
    '''
    rows = sensor.rows
    cols = sensor.cols
    
    # generate a random starting position (not too close to frame border)
    numb_targets = len(urobjs)
    offset = int(rows/6)
    sub_rows = rows-offset
    inds_y = np.random.random_integers(offset, rows-offset, numb_targets+1)
    inds_x = np.random.random_integers(offset, cols-offset, numb_targets+1)
    yx_pairs = [(inds_y[ii], inds_x[ii]) for ii in range(numb_targets+1)]
    yx_pairs = [x + np.random.rand(1) for x in yx_pairs] # a list of (y,x) floats

    # the last point in the list is the xing
    yx_xing = yx_pairs[-1]
    #yx_xing = np.array( (yx_pairs[0][0]+1.0, yx_pairs[0][1]+0.0) )
    #print( yx_pairs[:numb_targets] )
    #print( 'xing_point', yx_xing )

    # calculate the distances from each initial point to the xing
    dists = [ np.linalg.norm(yx_xing - pt)
              for pt in yx_pairs[:-1] ]
    #print('dists:', dists)

    # calculate the heading angles from each initial point to the xing
    angles = [ np.arctan2( ( yx_xing[0] - yx_pairs[ii][0] ),
                             yx_xing[1] - yx_pairs[ii][1] )
              for ii in range(numb_targets) ]
    #print('angles:', angles)

    # divide the distance to the xing into n_steps
    n_steps = random.choice( [x for x in range(10, 50)] )
    #print('n_steps', n_steps)

    # calculate the distance that each object must traverse to reach
    # the xing point in n_steps 
    trav_dists = [ d/n_steps for d in dists ]
    #print('trav_dists:', trav_dists)
    trav_incs = [ [ trav_dists[ii]*np.sin(angles[ii]),
                   trav_dists[ii]*np.cos(angles[ii]) ]
                 for ii in range(len(trav_dists)) ]
    #print('trav_incs:', trav_incs)

    # calculate the necessary (linear) way-points to the crossing
    post_xing_steps = 30
    positions = np.zeros( (n_steps+1+post_xing_steps, numb_targets, 2), dtype=np.float32 )

    for ii in range(positions.shape[0]):
        if ii==0:
            newposs = yx_pairs[:numb_targets]
        else:
            newposs = [ newposs_prev[jj] + trav_incs[jj]
                       for jj in range(numb_targets) ]
        newposs_prev = newposs
        #print(newposs)
        for itarg in range(numb_targets):
            positions[ii,itarg,:] = newposs[itarg]

    return(positions)
