import csv

def write(header_row, in_list):
    with open('yx_truth.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')#,
                 #quotechar='', quoting=csv.QUOTE_MINIMAL)
        if len(header_row)>0:
            writer.writerow(header_row)
        writer.writerows(in_list)
