# -*- coding: utf-8 -*-

__author__  = 'drforester'
__date__    = '19 February 2018'

'''
score_tracker.py

An example script showing how to use scorer.py which depends on py-motmetrics

The user must supply the following lists to the scorer:
    o_arrays - the object positions ground truth
    h_arrays - the hypothesis positions
    ground_truth_Ids - uh...the ground truth Ids
    hypotheses - the hypothesis ids

The scorer is invoked thusly:
   >> from utils import scorer    
   >> scorer.score_tracker(o_arrays, h_arrays, ground_truth_Ids, hypotheses)
'''

import pickle
import numpy as np
from utils import scorer    

truthIds = pickle.load( open('truth_Ids.pkl', 'rb') )
truthPos = np.load('truth_Pos.npy')

letters = ['a','b','c','d','e','f','g','h','i','j','k']
truth_letter_ids = { ii:letters[ii] for ii in range(len(truthPos[0]))}

# for testing, the truth and hypothesis are identical. Then changes are 
# made to the hypothesis to resemble the ouput of an imperfect traker.
o_arrays = truthPos.copy() 
h_arrays = truthPos.copy()
hypotheses = truthIds

ground_truth_Ids = []
for x in hypotheses:
    ground_truth_Ids.append( [truth_letter_ids[x[ii]] for ii in range(len(x))] )

########################################################
# Create some tracker errors
########################################################

# swap hypothesis track ids
temp_a = hypotheses[73][3]
temp_b = hypotheses[73][4]
hypotheses[73][3] = temp_b
hypotheses[73][4] = temp_a

temp_a = hypotheses[60][2]
temp_b = hypotheses[60][5]
hypotheses[60][3] = temp_b
hypotheses[60][1] = temp_a

# miss a track
h_arrays[80] = h_arrays[80][:-1]
hypotheses[80] = hypotheses[80][:-1]

# a false positive
false_detection = np.empty((1,2), dtype=np.float32)
false_detection[0,:] = np.array((20.0, 30.0))
h_arrays[89] = np.vstack( (h_arrays[89], false_detection) )
hypotheses[89].append(10)

#for ii in range(len(hypotheses)):
#    print( ii, ground_truth_Ids[ii], hypotheses[ii] )
#    print( ii, o_arrays[ii], h_arrays[ii] )
#    print('')

scorer.score_tracker(o_arrays, h_arrays, ground_truth_Ids, hypotheses)
